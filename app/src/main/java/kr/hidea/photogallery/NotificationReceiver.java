package kr.hidea.photogallery;

import android.app.Activity;
import android.app.Notification;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;

import kr.hidea.photogallery.util.Constants;

/**
 * Created by Eungi on 2017-12-06.
 */

public class NotificationReceiver extends BroadcastReceiver {
    private static final String TAG = Constants.TAG + NotificationReceiver.class.getSimpleName();

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.i(TAG, "receive result: " + getResultCode());
        if (getResultCode() != Activity.RESULT_OK) {
            // 포그라운드에서 실행되는 액티비티가 브로드캐스트 인텐트를 취소하였다.
            return;
        }
        int requestCode = intent.getIntExtra(PollService.REQUEST_CODE, 0);
        Notification notification = (Notification) intent.getParcelableExtra(PollService.NOTIFICATION);
        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);
        notificationManager.notify(requestCode, notification);
    }
}
