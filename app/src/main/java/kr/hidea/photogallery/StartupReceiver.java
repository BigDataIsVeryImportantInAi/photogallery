package kr.hidea.photogallery;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import kr.hidea.photogallery.util.Constants;

/**
 * Created by Eungi on 2017-12-06.
 */

public class StartupReceiver extends BroadcastReceiver {
    private static final String TAG = Constants.TAG + StartupReceiver.class.getSimpleName();
    @Override
    public void onReceive(Context context, Intent intent) {
        Log.i(TAG, "onReceive broadcast intent: " + intent.getAction());
        boolean isOn = QueryPreferences.isAlarmOn(context);
        PollService.setServiceAlarm(context, isOn);
    }
}
